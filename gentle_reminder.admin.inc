<?php

/**
 * @file
 * Admin settings.
 */


/**
 * Reminder settings.
 */
function gentle_reminder_admin_settings() {
  $form = array();

  $form['gentle_reminder_dev_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Development mode'),
    '#description' => t('Will not sent notifications by cron.'),
    '#default_value' => variable_get('gentle_reminder_dev_mode', FALSE),
  );

  $form['dev_mode_note'] = array(
    '#markup' => t("You may also want to add %code to your %file file.", array(
      '%code' => '$conf[\'gentle_reminder_dev_mode\'] = TRUE;',
      '%file' => 'settings.php',
    )),
  );

  $form['duration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Worker max duration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  foreach (module_implements('gr_worker') as $module) {
    $form['duration']['gentle_reminder_duration_' . $module] = array(
      '#type' => 'textfield',
      '#title' => 'queue_gr_' . $module,
      '#default_value' => variable_get('gentle_reminder_duration_' . $module, 30),
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
    );
  }

  return system_settings_form($form);
}
