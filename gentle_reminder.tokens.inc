<?php

/**
 * @file
 * Token callbacks for the Gentle Reminder module.
 */

/**
 * Implements hook_token_info().
 */
function gentle_reminder_token_info() {
  $tokens = array();

  // Collect main tokens.
  foreach (module_implements('gr_token_info') as $module) {
    foreach (module_invoke($module, 'gr_token_info') as $key => $token) {
      $tokens['gentle_reminder'][$key] = $token;
    }
  }

  $tokens['gentle_reminder']['items'] = array(
    'name' => t('Items'),
    'description' => t('The array of formatted items to send.'),
    'type' => 'gentle_reminder_items',
  );

  $tokens['gentle_reminder_items']['join'] = array(
    'name' => t('Join'),
    'description' => t('Items joined together.'),
  );

  $tokens['gentle_reminder_items']['count'] = array(
    'name' => t('Count'),
    'description' => t('The number of items.'),
  );

  return array(
    'types' => array(
      'gentle_reminder_items' => array(
        'name' => t('items'),
        'description' => t('The array of formatted items to send.'),
      ),

      'gentle_reminder' => array(
        'name' => t('Gentle reminder'),
        'description' => t('The recipient of the message.'),
        'needs-data' => 'gentle_reminder',
      ),
    ),
    'tokens' => $tokens,
  );
}

/**
 * Implements hook_tokens().
 */
function gentle_reminder_tokens($type, array $tokens, array $data = array(), array $options = array()) {
  $output = array();

  if ($type == 'gentle_reminder' && !empty($data['gentle_reminder'])) {
    // Process tokens form the core module.
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'items:count':
          $output[$original] = count($data['gentle_reminder']['items']);
          break;

        case 'items:join':
          $output[$original] = empty($data['gentle_reminder']['items'])
            ? ''
            : token_render_array($data['gentle_reminder']['items'], array('join' => '', 'sanitize' => FALSE) + $options);
          break;
      }
    }

    // Process tokens from hook_gr_token_info().
    foreach (module_implements('gr_token_info') as $module) {
      foreach (module_invoke($module, 'gr_token_info') as $prefix => $info) {
        if ($scope_tokens = token_find_with_prefix($tokens, $prefix)) {
          $scope_data = array(
            $info['type'] => $data['gentle_reminder'][$prefix],
          );

          foreach (token_generate($info['type'], $scope_tokens, $scope_data, $options) as $original => $value) {
            $output[$original] = $value;
          }
        }
      }
    }
  }

  return $output;
}
