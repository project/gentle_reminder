<?php

/**
 * Implements hook_gr_info().
 */
function hook_gr_info() {
  return array(
    'name' => t('Own nodes'),
    'item-type' => 'node',
    'item-needs-data' => 'node',
  );
}

/**
 * Implements hook_gr_token_info().
 *
 * Specify tokens for main message.
 */
function hook_gr_token_info() {
  return array(
    'node' => array(
      'name' => t('Just Node'),
      'description' => t('The node related to the message.'),
      'type' => 'node',
    ),
  );
}

/**
 * Implements hook_gr_queue().
 */
function hook_gr_queue() {
  $results = db_select('user', 'u')
    ->fields('u', array('uid'))
    ->execute()->fetchCol();

  return array_unique($results);
}

/**
 * Implements hook_gr_worker().
 */
function hook_gr_worker($uid) {
  $recipient = user_load($uid);
  $items = entity_load('node', FALSE, array('uid' => $recipient->uid));

  if ($recipient && $items) {
    gentle_reminder_send_mail($recipient, 'hook', array(
      'gentle_reminder' => array(
        'items' => $items,
        'node' => node_load(1), // Just random node.
      )
    ));
  }
}

/**
 * Implements hook_gr_template().
 */
function hook_gr_template() {
  return array(
    'subject' => t('You have some unread messages on [site:name]'),
    'body' => t("Hi [gentle_reminder:recipient:name],\n
You have [gentle_reminder:items:count] of [gentle_reminder:node:title]:\n[gentle_reminder:items:join]"),
    'item_body' => t("* [node:title]\n"),
  );
}
